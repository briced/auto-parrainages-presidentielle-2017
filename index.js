const fs = require('fs');

findCandidate = candidat => {
    return parrainage => {
        return candidat === `${parrainage['Nom']} ${parrainage['Prénom']}`;
    };
};

const datasStr = fs.readFileSync('./datas/parrainagestotal.json');

if(datasStr === undefined) {
    console.log(`Erreur reading parrainagestotal.json`);
    return;
}

const datas = JSON.parse(datasStr);
const htmlList = datas
    .map(data => {
        return {
            nbParrainage: data['Parrainages'].length,
            parrainage: data['Parrainages'].find(findCandidate(data['Candidat-e parrainé-e']))
        }
    })
    .filter(data => data.parrainage!==undefined)
    .map(data => `${data.parrainage['Nom']} ${data.parrainage['Prénom']} (${data.parrainage['Mandat']} de ${data.parrainage['Département']}) : ${data.nbParrainage} parrainage${data.nbParrainage!==1?'s':''}`)
    .map(fullName => {
        console.log(fullName);
        return fullName;
    })
    .reduce((acc, fullName) => `${acc}<li>${fullName}</li>`,'');

const html = fs.readFileSync('./template/index.html').toString();
const newHtml = html.replace('<div id="resultats">',`<div id="resultats">\n<ul>${htmlList}</ul>`);

fs.mkdir('./public/',function(e){
    if(!e || (e && e.code === 'EEXIST')){
        fs.writeFileSync('./public/index.html', newHtml);
    }
});
